<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\User;
use App\Comment;
class PostsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');                
    }
    public function index()
    {
        
        return view('post');
    }
    public function create(Request $request){        
        $imageName ='images/'.time() . '.' . $request->file('image')->getClientOriginalExtension();
        $request['image']->move(base_path() . '/public/images/', $imageName);       
                
        $post = new Post();
        $post->user_id= $request->user_id;
        $post->caption=$request->caption;
        $post->image=$imageName;        
        $post->save();
        return redirect('home');


    }
    public function like($id){
        Post::find($id)->increment('likes');        
        return redirect('home');
    }
    public function detail($id){
        $posts = Post::find($id);
        // return view('detail',['detail'=>$posts]);
        return view('detail',['posts'=>$posts]); 
    }
    public function comment(Request $request ){        
    //    echo $request->comment; 
        $comment = new  Comment();
        $comment->user_id =  $request->user_id;
        $comment->post_id = $request->post_id;
        $comment->comment = $request->comment;
        $comment->save();
        return redirect('home');
    }
    public function detail_comment(Request $request ){        
    //    echo $request->comment; 
        $comment = new  Comment();
        $comment->user_id =  $request->user_id;
        $comment->post_id = $request->post_id;
        $comment->comment = $request->comment;
        $comment->save();
        return redirect('detail');
    }
}

