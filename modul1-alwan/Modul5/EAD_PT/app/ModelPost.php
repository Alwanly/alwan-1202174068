<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPost extends Model
{
    protected$table='posts';
    protected $fillable =  ['user_id','caption','image'];
}
