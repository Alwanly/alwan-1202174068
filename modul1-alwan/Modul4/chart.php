<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include 'proses/session.php';     
    ?>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>        
    <title>Chart</title>  
    <?php       
    include "proses/connection.php";
    $id=$_SESSION['id'];    
    $result=mysqli_query($con,"SELECT * FROM cart_table WHERE user_id='$id'"); 
    $total = 0;               
    ?>  
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <div class="container-fluid">
            <div class="col-12">
                <div class="row">
                    <img src="image/EAD.png" alt="EAD" class="EAD">
                    <ul class="nav navbar-nav navbar-right nav-index">
                        <?php                
                        if(!isset($_SESSION['user'])){
                        echo "<li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#Modal-login'>Login</a></li>
                        <li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#Modal-registrasi'>Registrasi</a></li>";
                        }
                        else{
                        echo "<li class='nav-item dropdown' >
                        <a class='nav-link dropdown-toggle' data-toggle='dropdown' id='navbarDropdownMenuLink'
                        href='#' role='button' aria-haspopup='true' aria-expanded='false'>".$_SESSION['user']."</a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                            <a class='dropdown-item' href='index.php'>Home</a>
                            <a class='dropdown-item' href='proses/logout.php'>Logout</a>
                            </div>                    
                            </li>              
                            " ;
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fuild"> 
            <div class="row justify-content-md-center">
                <div class="col-10" style="text-align:center;">
                    <h2>Cart</h2>    
                </div>
            </div>
            <div class="row justify-content-md-center">                
                <div class="col-8">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">                
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>product</th>
                                        <th>price</th>
                                        <th>Action  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row=mysqli_fetch_array($result)) {
                                        echo "<tr>";
                                                echo "<td>".$row['id']."</td>";
                                                echo "<td>".$row['product']."</td>";
                                                echo "<td>".$row['price']."</td>";
                                                echo "
                                                <td><a class='btn btn-danger' href='proses/delete.php?id=".$row['id']."'>X</a></td>";                                
                                                $total += $row['price'];
                                            ?>
                                            </tr>                           
                                    <?php } ?>
                                    <tr>
                                    <td colspan="2" style="text-align:center;">Total</td>                                    
                                    <td><?php echo $total ?></td>
                                    <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                     
            <div class="row  justify-content-md-center">
            <!-- Footer -->
            <footer class="page-footer font-small cyan darken-3">
            <!-- Footer Elements -->
            <div class="container">
                <!-- Grid row-->
                <div class="row">
                <!-- Grid column -->
                <div class="col-md-8 py-5">
                    <div class="mb-5 flex-center">
                    <!-- Facebook -->                    
                    </div>
                </div>
                <!-- Grid column -->
                </div>
                <!-- Grid row-->
            </div>
            <!-- Footer Elements -->
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">© 2019 Copyright:
                <a href="https://www.instagram.com/alwanly"> instagram.com</a>
            </div>
            <!-- Copyright -->
            </footer>
            <!-- Footer -->
        <div> 
    </div>
</body>
</html>
