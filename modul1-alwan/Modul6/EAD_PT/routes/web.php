<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/post/comment/', 'PostsController@comment')->name('post.comment');
// Route::post('/post/comment/', 'PostsController@detail_comment')->name('post.detail_comment');
Route::get('/post','PostsController@index')->name('post');
Route::post('/post/create','PostsController@create')->name('post.create');
Route::get('/post/like/{id}','PostsController@like')->name('post.like');
Route::get('/post/detail/{id}','PostsController@detail')->name('post.detail');
// Route::post('/post/comment','PostsController@comment')->name('post.comment');
Route::get('/profile/{id}','ProfilController@index')->name('profile');
Route::post('/profile/update','ProfilController@update')->name('profile.update');