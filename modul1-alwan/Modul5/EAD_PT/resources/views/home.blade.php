@extends('layouts.app')

@section('content')
<div class="container">
    @if(!isset($posts))
   <div class="row justify-content-center">        
        <div class="col-5">                        
            <div class="card">                                
                <div class="card-header bg-danger"> belum ada </div>
                <div class="card-body">                
                </div>
                
                <div class="card-footer bg-warning">
                    <p class="card-text">Belum ada</p>
                </div>                
            </div>                                
        </div>
    </div>        
    @else
    @foreach ($posts as $post)
    <div class="row justify-content-center">        
        <div class="col-lg-5 col-xs--12">                        
            <div class="card">                                
                <div class="card-header bg-dark text-white">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ Auth::user()->avatar }}" alt="" style="border-radius: 50%; width:100%; " srcset="">
                        </div> 
                        <div class="col-8">
                            <p>{{ Auth::user()->name }} </p>
                        </div>
                    </div>
                </div>                
                <img src="{{ $post->image }}" class="mx-auto " alt="Logo" style="width:100%; height:350;">                                
                <div class="card-footer bg-light">
                    <h6 class="card-text">{{ Auth::user()->title }}</h6>
                    <p class="card-text">{{ $post->caption }}</p>
                </div>                
            </div>                                
        </div>
    </div>
    @endforeach
    @endif
</div>
</div>
@endsection

<style>
.row{
    margin-bottom:15px;
}
</style>