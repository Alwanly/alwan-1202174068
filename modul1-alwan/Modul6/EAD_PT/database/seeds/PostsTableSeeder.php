<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('posts')->insert([
             'user_id'=> 2,
              'caption'=>'Happy In Daspro',
              'image'=>'images/daspro.jpg',
              'likes'=>20,             
        ]); 
        DB::table('posts')->insert([
             'user_id'=> 2,
              'caption'=>'Happy In class',
              'image'=>'images/class.jpg',             
              'likes'=>20,
        ]); 
        DB::table('posts')->insert([
             'user_id'=> 1,
              'caption'=>'Happy In prodase',
              'image'=>'images/prodase.jpg',             
              'likes'=>20,
        ]); 
    }
}
