@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Post') }}</div>
                
                <div class="card-body">
                    <form method="POST" action="{{ route('post.create') }}" enctype="multipart/form-data">       
                    {{ csrf_field() }}                                                               
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('caption') }}</label>
                            <input type="hidden" name="user_id" value='<?php echo Auth::user()->id?>'>
                            <div class="col-md-6">
                                <input id="caption" type="text" class="form-control" name="caption" required autocomplete="caption">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Photo') }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" required autocomplete="image">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Post') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 