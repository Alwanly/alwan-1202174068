@extends('layouts.app')

@section('content')

<div class="row justify-content-md-center">
    <div class="col-10 ">
        <div class="card mb-12" style="max-width: 100%;">
        <div class="row no-gutters">
            <div class="col-9">
            <img src="../../{{ $posts->image }} " class="card-img" alt="..." style="height: 500px;">
            </div>
            <div class="col-3">
            <div class="card-body">                
                <h5 class="card-title">{{ $posts->users['name'] }}</h5>
                <p class="card-text">{{ $posts->caption }}</p>
                @foreach($posts->comments as $cm)
                <p class="card-text"><small class="text-muted">{{ $cm->user['name'] }}:{{ $cm['comment'] }} </small></p>
                @endforeach
                 <form action="{{ route('post.comment') }}" method="post">
                        {{ csrf_field() }}  
                               
                        <div class="row">
                            <div class="col-9">
                                <input class="form-control form-control-sm" type="hidden" name="post_id" value="{{$posts->id}}">
                                <input class="form-control form-control-sm" type="hidden" name="user_id" value="{{ Auth::user()->id }}">                                
                                <input class="form-control form-control-sm" type="text" name="comment" placeholder="Comment">
                            </div>
                            <div class="col-2">
                                <button class="btn btn-outline-secondary btn-sm" type="submit"
                                    id="button-addon2">POST</button>
                            </div>
                        </div>
                    </form>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>

@endsection