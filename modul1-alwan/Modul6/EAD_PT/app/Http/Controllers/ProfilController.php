<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
class ProfilController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');                
    
    }  
    public function index($id)
    {        
        $posts = Post::where('user_id',$id)->get();
        return view('profile',['posts'=>$posts]);
    }
    public function update(Request $request){        
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email= $request->email;
        $user->title = $request->title;
        $user->url = $request->url;
        $user->description = $request->description;        
        $user->save();
         return redirect('home');

    }
}
