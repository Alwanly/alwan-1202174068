@extends('layouts.app')

@section('content')
<div class="container">
    <div class="continter-fuild">
        <div class="row justify-content-lg-center">
            <div class="col-3">
                <img src="  ../{{ Auth::user()->avatar}} " alt="profile" class="avatar">
            </div>
            <div class="col-4">
                <b>{{ Auth::user()->name}}</b>
                <p><a href="#s" data-toggle="modal" data-target="#exampleModal">Edit Profil</a></p>
                <p>{{ Auth::user()->title}}</p>
                <p>{{ Auth::user()->url}}</p>
                <p>{{ Auth::user()->description}}</p>
            </div>
            <div class="col-2">
                <a href="{{ url('/post') }}">add NewPost</a>
            </div>
        </div>
    </div>
    <b>
        <hr></b>
    <div class="container">
        <div class="row">
            @foreach($posts as $post)
            <div class="col-3">
                <div class="card" style="width: 100%;height:16  rem">
                    <img src="../{{$post->image}}" class="card-img-top mx-auto" alt="dd"
                        style="width: 100%;height:200px">
                    <div class="card-footer">
                        <b class="card-title">{{$post->caption}}</b>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ Auth::user()->name}}" required autocomplete="name" autofocus>
                    
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email"
                            class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ Auth::user()->email}}" required autocomplete="email">
                        
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password">                         
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ Auth::user()->title}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description"
                            class="col-md-4 col-form-label text-md-right">{{ __('URL') }}</label>

                        <div class="col-md-6">
                            <input id="description" type="text" class="form-control" name="url" value="{{ Auth::user()->url}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description"
                            class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                        <div class="col-md-6">
                            <input id="description" type="text" class="form-control" name="description" value="{{ Auth::user()->description}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Avatar') }}</label>

                        <div class="col-md-6">
                            <input id="avatar" type="file" class="form-control" name="avatar" value="{{ Auth::user()->avatar}}">
                        </div>
                    </div>
                    <input id="avatar" type="hidden" class="form-control" name="avatar" required autocomplete="avatar"
                        value=''>
                        <input id="id" type="hidden" class="form-control" name="id" required autocomplete="avatar"
                        value='{{Auth::user()->id}}'>
                        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
    .avatar {
        vertical-align: middle;
        width: 200px;
        height: 200px;
        border-radius: 50%;
    }

    .card {
        margin-bottom: 20px;
    }
</style>