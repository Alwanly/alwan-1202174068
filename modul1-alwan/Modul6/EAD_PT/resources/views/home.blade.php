@extends('layouts.app')

@section('content')
<div class="container">
    @if(!isset($posts))
    <div class="row justify-content-center">
        <div class="col-5">
            <div class="card">
                <div class="card-header bg-danger"> belum ada </div>
                <div class="card-body">
                </div>

                <div class="card-footer bg-warning">
                    <p class="card-text">Belum ada</p>
                </div>
            </div>
        </div>
    </div>
    @else
    @foreach($posts as $post)
    <div class="row justify-content-center">
        <div class="col-lg-6 col-xs-12">
            <div class="card">
                <div class="container-fuild">
                    <div class="card-header bg-dark text-white">

                        <img src="{{ $post->users['avatar'] }}" alt="profil"
                            style="border-radius: 50%; width:35px; height:35px; " srcset="">
                        {{ $post->users['name'] }}

                    </div>
                </div>
               <a href="{{ route('post.detail',['id'=>$post->id]) }}"><img src="{{ $post->image }}" class="mx-auto " alt="Logo" style="width:100%; height:350;"></a>
                <div class="card-footer bg-light">
                    <div class="row">
                        <div class="col-1">
                            <a href="{{ route('post.like',['id'=>$post->id]) }}" class="material-icons">&#xe87e;</a>
                        </div>
                        <div class="col-1">
                            <a href="" class="fa">&#xf0e5;</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {{ $post->likes }} Likes
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            {{ $post->caption }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-10">
                            
                            <div class="row">
                                @foreach($post->comments->take(3) as $cm)
                                <div class="col-12">
                                {{$cm->user['name']}} :{{$cm['comment']}}
                                </div>
                                @endforeach
                            </div>
                            
                        </div>
                    </div>
                    <form action="{{ route('post.comment') }}" method="post">
                        {{ csrf_field() }}  
                               
                        <div class="row">
                            <div class="col-10">
                                <input class="form-control form-control-sm" type="hidden" name="post_id" value="{{$post->id}}">
                                <input class="form-control form-control-sm" type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input class="form-control form-control-sm" type="text" name="comment" placeholder="Comment">
                            </div>
                            <div class="col-2">
                                <button class="btn btn-outline-secondary btn-sm" type="submit"
                                    id="button-addon2">POST</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>    
    @endforeach
    @endif
</div>
</div>
@endsection

<style>
    .row {
        margin-bottom: 15px;
    }
</style>