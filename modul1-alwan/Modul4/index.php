<!DOCTYPE html>
<html lang="en">
<?php
//include "proses/session.php";
session_start();
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <title>Index</title>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <div class="container-fluid">
            <div class="col-12">
                <div class="row">
                    <img src="image/EAD.png" alt="EAD" class="EAD">
                    <ul class="nav navbar-nav navbar-right nav-index">
                        <?php                
                        if(!isset($_SESSION['user'])){
                            ?>
                        <li><a class='nav-item nav-link' href='#' data-toggle='modal'
                                data-target='#Modal-login'>Login</a></li>
                        <li><a class='nav-item nav-link' href='#' data-toggle='modal'
                                data-target='#Modal-registrasi'>Registrasi</a></li>
                        <?php  }else{ ?>
                        <li><a class='nav-item nav-link' href='chart.php'><img src="image/cart.png" alt="cart"></a></li>
                        <li class='nav-item dropdown'>
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdownMenuLink"
                                href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo $_SESSION['user'] ?> </a>
                            <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                                <a class='dropdown-item' href='edit_profile.php'>Setting</a>
                                <a class='dropdown-item' href='proses/logout.php'>Logout</a>
                            </div>
                        </li>
                        <?php  }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid col-9">
                <div class="banner">
                    <h1>Hello Coders</h1>
                    <p>Welcome to our store, please take a look for the products you might buy</p>
                </div>
                <div class="product">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card" style="width: 18rem;">
                                <img src="image/WebPrograming.jpg" class="card-img-top" alt="Web">
                                <div class="card-body">
                                    <h5 class="card-title">Learning Basic Web Programming Rp.200.000</h5>
                                    <p class="card-text">Biaya Sangat Terjangkau, Dapat Kesempatan Kerja, Dapat
                                        Sertifikat Skala
                                        Internasional. Materi Sesuai Dengan Kebutuhan Lapangan. Kelas: Programming.</p>
                                </div>

                                <div class="card-body">
                                    <a href="proses/buy.php?id=1&product=Learning+Basic+Web+Programming&price=200000"
                                        class="btn btn-primary" style="width:100%;">Buy</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style="width: 18rem;">
                                <img src="image/Java Programming.jpg" class="card-img-top" alt="Java">
                                <div class="card-body">
                                    <h5 class="card-title">Starting Programming in Java Rp.150.000</h5>
                                    <p class="card-text">Java is one of the most popular and widely used programming
                                        language and platform.
                                        A platform is an environment that helps to develop and run programs.</p>
                                </div>
                                <div class="card-body">
                                    <a href="proses/buy.php?product=Starting+Programming+in+Java&price=150000"
                                        class="btn btn-primary" style="width:100%;">Buy</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style="width: 18rem;">
                                <img src="image/python.jpg" class="card-img-top" alt="Python">
                                <div class="card-body">
                                    <h5 class="card-title">Starting Programming in Python Rp.200.000</h5>
                                    <p class="card-text">The Python logo is a trademark of the Python Software
                                        Foundation,
                                        which is responsible for defending against any damaging or confusing uses of the
                                        trademark.</p>
                                </div>
                                <div class="card-body">
                                    <a href="proses/buy.php?product=Starting+Programming+in+Python&price=200000"
                                        class="btn btn-primary" style="width:100%;">Buy</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                </div>
            </div>
            <div class="modal fade" id="Modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="proses/login.php" method="POST">
                                <div class="form-group">
                                    <label for="Email_Address">Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        placeholder="Email input">
                                </div>
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                        placeholder="Password input">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" id="submit" name="submit" value="Login"
                                        class="btn btn-primary">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="Modal-registrasi" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Registrasi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="proses/registrasi.php" method="POST">
                                <div class="form-group">
                                    <label for="Email_Address">Email Address</label>
                                    <input type="email" class="form-control" name="email" placeholder="Input Email">
                                </div>
                                <div class="form-group">
                                    <label for="Username">Username</label>
                                    <input type="text" class="form-control" name="username"
                                        placeholder="Input Username">
                                </div>
                                <div class="form-group">
                                    <label for="Number">Number HandPhone</label>
                                    <input type="number" class="form-control" name="number"
                                        placeholder="Input Number HandPhone">
                                </div>
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Regis</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row  justify-content-md-center">
            <!-- Footer -->
            <footer class="page-footer font-small cyan darken-3">
                <!-- Footer Elements -->
                <div class="container">
                    <!-- Grid row-->
                    <div class="row">
                        <!-- Grid column -->
                        <div class="col-md-8 py-5">
                            <div class="mb-5 flex-center">
                                <!-- Facebook -->
                            </div>
                        </div>
                        <!-- Grid column -->
                    </div>
                    <!-- Grid row-->
                </div>
                <!-- Footer Elements -->
                <!-- Copyright -->
                <div class="footer-copyright text-center py-3">© 2019 Copyright:
                    <a href="https://www.instagram.com/alwanly"> instagram.com</a>
                </div>
                <!-- Copyright -->
            </footer>
            <!-- Footer -->
            <div>
            </div>
</body>

</html>