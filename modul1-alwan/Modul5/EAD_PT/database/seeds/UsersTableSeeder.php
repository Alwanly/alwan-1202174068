<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // for ($i=0; $i < 10; $i++) { 
            
        DB::table('users')->insert([
              'name'  => 'Alwan',
             'email' => 'Alwan@mail.com',
             'password'  => bcrypt('12345678'),
             'title' =>"Happy on Telkom University",
             'description' =>"Always Grateful",
             'avatar'=>"images/avatar.jpg",
        ]); 
        // }
         DB::table('users')->insert([
              'name'  => 'ley',
             'email' => 'ley@mail.com',
             'password'  => bcrypt('12345678'),
             'title' =>"Happy on Telkom University",
             'description' =>"Ley Grateful",
             'avatar'=>"images/avatar.jpg",
        ]); 
    }   
}
