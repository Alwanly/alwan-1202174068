<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\ModelPost;
class PostsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');                
    }
    public function index()
    {
        
        return view('post');
    }
    public function create(Request $request){
        // $image = $request->file('image');
        // $name_file= $image->getClientOriginalName();       
        // Storage::disk('public')->put($name_file,  File::get($image));
        $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
        $request['image']->move(base_path() . '/public/uploads/', $imageName);
       
        $post = new ModelPost();
        $post->user_id= $request->user_id;
        $post->caption=$request->caption;
        $post->image= $imageName;
        $post->save();
        return redirect('home');

    }
}
