<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "Posts";
    protected $fillable = [
        'id','user_id','caption','image','create_at','update_at',
    ];

     public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}

